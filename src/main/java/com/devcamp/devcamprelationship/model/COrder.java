package com.devcamp.devcamprelationship.model;

import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "orders")
public class COrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "create_at")
    private Date createdAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    private CCustomer customer;

    @OneToOne(fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            mappedBy = "order")
    private CPayment payment;
    
    public COrder() {
    }

    public COrder(long id, Date createdAt, CCustomer customer, CPayment payment) {
        this.id = id;
        this.createdAt = createdAt;
        this.customer = customer;
        this.payment = payment;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public CCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(CCustomer customer) {
        this.customer = customer;
    }

    public CPayment getPayment() {
        return payment;
    }

    public void setPayment(CPayment payment) {
        this.payment = payment;
    }
    
   
}
